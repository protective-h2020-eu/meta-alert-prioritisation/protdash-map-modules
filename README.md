# ProtDash-MAP-modules

Implementations of Data Provider, Data Provider Editor, Mapping Provider (for Basic Table VP)  to fetch data from criteria-mapper MAP module. 
Small changes to basic-table-vp-component.html and .css has been introduced to show simple tabular view with cells colored in accordance with priority.
